#define TSOCKET_INTERNAL

/**
 * @defgroup threaded_socket
 * @{
 */

#include "threaded_socket_data.h"
#include "socket.h"

#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <glib-2.0/glib.h>

static u32 id = 0;

/**
 * Creates a client from a server
 * @param client Client sock to bind to
 * @param server Server socket to bind from
 * @return TSOCKET_OK if everything went ok, otherwise return error
 */
t_tsocket *init_tsocket_client_from_server(t_socket *server, t_tsocket_status *status) {
    socket_status sock_status;
    t_tsocket *client;

    client = g_malloc0(sizeof(t_tsocket));
    if (!client)
        return set_status_and_return(TSOCKET_MALLOC, status, NULL);
    sock_status = new_client_from_server(&client->sock, server);
    switch (sock_status) {
        case SOCKET_ARGS:
            return set_status_and_return(TSOCKET_ARGS, status, NULL);
        case SOCKET_SOCKET:
        case SOCKET_OPT:
        case SOCKET_IP:
        case SOCKET_BIND:
            return set_status_and_return(TSOCKET_BIND, status, NULL);
        default:
            break;
    }
    set_flags_socket(&client->sock, O_NONBLOCK);
    client->input = g_string_new(NULL);
    if (client->input == NULL)
        return set_status_and_return(TSOCKET_MALLOC, status, NULL);
    client->output = g_queue_new();
    if (client->output == NULL)
        return set_status_and_return(TSOCKET_MALLOC, status, NULL);;
    client->id = id;
    id++;
    return set_status_and_return(TSOCKET_OK, status, client);
}

/**
 * Create new client
 * @param client Client socket to bind to
 * @param address Address to bind to
 * @param port Port to bind to
 * @return TSOCKET_OK if everything went ok, otherwise return error
 */
t_tsocket *init_tsocket_client(const s8 *address, u16 port, t_tsocket_status *status) {
    socket_status sock_status;
    t_tsocket *client;

    client = g_malloc0(sizeof(t_tsocket));
    if (!client)
        return set_status_and_return(TSOCKET_MALLOC, status, NULL);
    sock_status = new_client(&client->sock, address, port);
    switch (sock_status) {
        case SOCKET_ARGS:
            return set_status_and_return(TSOCKET_ARGS, status, NULL);
        case SOCKET_SOCKET:
        case SOCKET_OPT:
        case SOCKET_IP:
        case SOCKET_BIND:
            return set_status_and_return(TSOCKET_BIND, status, NULL);
        default:
            break;
    }
    set_flags_socket(&client->sock, O_NONBLOCK);
    client->input = g_string_new(NULL);
    if (client->input == NULL)
        return set_status_and_return(TSOCKET_MALLOC, status, NULL);
    client->output = g_queue_new();
    if (client->output == NULL)
        return set_status_and_return(TSOCKET_MALLOC, status, NULL);
    client->id = id;
    id++;
    return set_status_and_return(TSOCKET_OK, status, client);
}

/**
 * @}
 */
/**
 * @defgroup Socket
 * @{
 */

#ifndef SOCKET_H
#define SOCKET_H

#include "number.h"

#include <stdbool.h>
#include <arpa/inet.h>


struct s_socket {
    /** File descriptor */
    s32 socket_fd;
    /** Address binded to */
    struct sockaddr_in address_info;
};

enum e_socket_status {
    SOCKET_SOCKET = 1,
    SOCKET_OPT = 2,
    SOCKET_IP = 3,
    SOCKET_PORT = 4,
    SOCKET_BIND = 5,
    SOCKET_ARGS = 6,
    SOCKET_MALLOC = 7,
    SOCKET_FD = 8,
    SOCKET_OK = 0
};

typedef struct s_socket t_socket;
typedef enum e_socket_status socket_status;


socket_status new_server(t_socket *server, const s8 *address, u16 port, u32 clients);

socket_status new_client_from_server(t_socket *client, t_socket *server);

socket_status new_client(t_socket *socket_p, const s8 *address, u16 port);

socket_status send_data_socket(s32 fd, const s8 *data, u64 data_size);

socket_status receive_data_socket(s32 fd, u64 data_size, s8 *data_ptr, u64 *bytes_read);

socket_status set_flags_socket(t_socket *socket, int flags);

s32 get_fd_socket(t_socket *sock);

u8 *get_socket_address(t_socket *sock);

const u8 *get_string_socket_status(socket_status status);

void close_socket(t_socket *client);

/**
 * @}
 */

#endif

#define TSOCKET_INTERNAL

/**
 * @defgroup threaded_socket
 * @{
 */

#include "threaded_socket.h"
#include "threaded_socket_time.h"
#include "threaded_socket_mutex.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/**
 * Adds unterminated data to queue to be sent.
 * @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
 * @param sock The socket
 * @param data Data to be sent
 * @param length Length of the data
 * @return Other status if failed, or TSOCKET_OK if succeeded.
 */
t_tsocket_status
send_unterminated_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, const u8 *data, u64 length) {
    u8 *copy;
    t_tsocket_status status;

    struct s_packet_data *string;

    if (!sock || !data)
        return TSOCKET_ARGS;
    copy = malloc(length);
    if (!copy)
        return TSOCKET_MALLOC;
    memcpy(copy, data, length);
    string = malloc(sizeof(struct s_packet_data));
    if (!string)
        return TSOCKET_MALLOC;
    string->length = length;
    string->data = copy;
    if (manager) {
        status = lock_tsocket_manager(manager, sock);
        if (status != TSOCKET_OK)
            return status;
    }
    g_queue_push_tail(sock->output, string);
    if (manager) {
        status = unlock_tsocket_manager(manager, sock);
        if (status != TSOCKET_OK)
            return status;
    }
    return TSOCKET_OK;
}

/**
 * Adds terminated data to queue to be sent.
 * @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
 * @param sock The socket
 * @param data Data to be sent
 * @return Other status if failed, or TSOCKET_OK if succeeded.
 */
t_tsocket_status send_terminated_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, const u8 *data) {
    if (!data)
        return TSOCKET_ARGS;
    return send_unterminated_data_tsocket(manager, sock, data, strlen((char *) data));
}

/**
 * Returns the amount of bytes currently available.
 * @param sock The socket
 * @return How many bytes currently available.
 */
u64 data_available_tsocket(t_tsocket *sock) {
    if (!sock)
        return 0;
    return sock->input->len;
}

/**
 * If there is data received, return it and reset the buffer.
 * Otherwise set the status to TSOCKET_NO_DATA and return NULL.
 * @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
 * @param sock The socket
 * @param status Status pointer
 * @return NULL if failed, or the current data.
 */
u8 *get_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, u64 *bytes, t_tsocket_status *status) {
    u8 *data;

    if (sock->input->len == 0) {
        *status = TSOCKET_NO_DATA;
        return NULL;
    }
    *bytes = sock->input->len;
    if (manager) {
        *status = lock_tsocket_manager(manager, sock);
        if (*status != TSOCKET_OK)
            return NULL;
    }
    data = (u8 *) g_string_free(sock->input, false);
    sock->input = g_string_new(NULL);
    if (manager) {
        *status = unlock_tsocket_manager(manager, sock);
        if (*status != TSOCKET_OK)
            return NULL;
    }
    if (sock->input == NULL) {
        *status = TSOCKET_MALLOC;
        return NULL;
    }
    if (data == NULL) {
        *status = TSOCKET_NO_DATA;
        return NULL;
    }
    *status = TSOCKET_OK;
    return data;
}

/**
 * If there is data received, return it.
 * Otherwise set the status to TSOCKET_NO_DATA and return NULL.
 * @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
 * @param sock The socket
 * @param status Status pointer
 * @return NULL if failed, or the current data.
 */
t_tsocket_status peek_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, u8 *data, u64 bytes) {
    u32 i;
    t_tsocket_status status;

    if (!data)
        return TSOCKET_ARGS;
    if (sock->input->len == 0)
        return TSOCKET_NO_DATA;
    if (manager) {
        status = lock_tsocket_manager(manager, sock);
        if (status != TSOCKET_OK)
            return status;
    }
    for (i = 0; i < sock->input->len && i < bytes; i++)
        data[i] = sock->input->str[i];
    data[i] = 0;
    if (manager) {
        status = unlock_tsocket_manager(manager, sock);
        if (status != TSOCKET_OK)
            return status;
    }
    return TSOCKET_OK;
}

/**
 * Returns the socket file descriptor.
 * @param sock tSocket
 * @return File descriptor
 */
u64 get_tsocket_fd(t_tsocket *sock) {
    return sock->sock.socket_fd;
}

/**
 * Returns the address the socket is currently bound to. If not bound, will return garbage.
 * This function will malloc for the total size and return that. Use this if you want a thread safe copy.
 * @param sock Socket
 * @param status Status pointer
 * @return Address in address:port form
 */
u8 *get_tsocket_address(t_tsocket *sock, t_tsocket_status *status) {
    u8 *address;
    u8 *combined;

    if (!status)
        return NULL;
    if (!sock) {
        *status = TSOCKET_ARGS;
        return NULL;
    }
    address = (u8 *) inet_ntoa(sock->sock.address_info.sin_addr);
    combined = malloc(5 + strlen((char *) address) + 1);
    if (!combined) {
        *status = TSOCKET_MALLOC;
        return NULL;
    }
    sprintf((char *) combined, "%s:%d", address, sock->sock.address_info.sin_port);
    return combined;
}

/**
 * Returns the address the socket is currently bound to. If not bound, will return garbage.
 * This function will malloc for the total size and return that. Use this if you want a temporary not thread safe copy.
 * @param sock Socket
 * @param status Status pointer
 * @return Address in address:port form
 */
u8 *get_tsocket_address_tmp(t_tsocket *sock) {
    u8 *address;
    static u8 combined[31];

    if (!sock)
        return NULL;
    address = (u8 *) inet_ntoa(sock->sock.address_info.sin_addr);
    sprintf((char *) combined, "%s:%d", address, sock->sock.address_info.sin_port);
    return combined;
}

/**
 * Returns the socket of a tsocket
 * @param sock tsocket
 * @return t_socket
 */
t_socket *get_tsocket_socket(t_tsocket *sock) {
    if (!sock)
        return NULL;
    return &sock->sock;
}

/**
 * Closes the socket, and frees the queue and vector.
 * Will block until there is no more data. If your socket is attached to a manager,
 * and you remove the socket from the manager before this call, it may block infinitely.
 * If you are running this without a manager, you can run the update loop yourself to prevent this from happening.
 * @param sock Socket to destroy
 * @return TSOCKET_ARGS if the socket is null, TSOCKET_OK otherwise
 */
t_tsocket_status destroy_tsocket(t_tsocket *sock) {
    if (!sock)
        return TSOCKET_ARGS;
    if (!sock->output || !sock->input)
        return TSOCKET_ARGS;
    while (sock->output->length > 0)
        delay(1);
    close_socket(&sock->sock);
    g_string_free(sock->input, true);
    g_queue_free_full(sock->output, free);
    g_free(sock);
    return TSOCKET_OK;
}

u8 *get_version_string(void) {
    static u8 version[19];

    sprintf((char *)version, "%d.%d.%d", MAJOR, MINOR, BUILD);
    return version;
}

/**
 * Returns the string version of the tsocket status
 * @param status tsocket status
 * @return String version of status
 */
const u8 *tsocket_status_string(t_tsocket_status status) {
    switch (status) {
        case TSOCKET_NO_DATA:
            return (const u8 *) "There is no data waiting.";
        case TSOCKET_ARGS:
            return (const u8 *) "Invalid arguments.";
        case TSOCKET_MALLOC:
            return (const u8 *) "Out of memory.";
        case TSOCKET_THREAD:
            return (const u8 *) "Failed to create thread.";
        case TSOCKET_BIND:
            return (const u8 *) "Failed to bind to ip and port."\
            "Is there another process running on that port?";
        case TSOCKET_OK:
            return (const u8 *) "All good.";
        default:
            return (const u8 *) "Unknown status.\n";
    }
}

/**
 * @}
 */
/**
 * @defgroup threaded_socket_time
 * @{
 */

#include "threaded_socket_time.h"

#include <sys/time.h>
#include <time.h>
#include <unistd.h>

/**
 * Gets the current time from the epoch in MS.
 * @return MS since the epoc.
 */
u64 get_current_time(void) {
    struct timeval tp;

    gettimeofday(&tp, NULL);
    return tp.tv_sec * 1000 + tp.tv_usec / 1000;
}

/**
 * Delays for MS
 * @param ms How long to wait
 */
void delay(u64 ms) {
    struct timespec sleep_time, elapsed;

    sleep_time.tv_sec = ms / 1000;
    sleep_time.tv_nsec = (ms % 1000) * 1000000;
    nanosleep(&sleep_time, &elapsed);
}

/**
 * @}
 */
//
// Created by kohlten on 4/26/19.
//

#define TSOCKET_INTERNAL

#include "threaded_socket_manager.h"
#include "threaded_socket_time.h"
#include "threaded_socket.h"

#include <unistd.h>
#include <stdio.h>

/* @TODO Since the mutex locks per one thread,
  it might be an issue later when we are trying
  to send data from a client on one thread and
  reading from another client on the same thread.

  Could create a semaphore for each socket.
  This would depend on how expensive that would be.

  Also that we should probably find a better way to locate the sockets.
*/

void *set_status_and_return(t_tsocket_status status, t_tsocket_status *status_ptr, void *value) {
    *status_ptr = status;
    return value;
}

static t_tsocket_status set_up_threads(t_tsocket_manager *manager) {
    u32 i;

    for (i = 0; i < manager->threads_length; i++) {
        manager->thread_data[i].mutex = new_mutex();
        if (!manager->thread_data[i].mutex)
            return TSOCKET_MALLOC;
        manager->thread_data[i].socks = g_ptr_array_new();
        if (!manager->thread_data[i].socks)
            return TSOCKET_MALLOC;
        manager->thread_data[i].status = TSOCKET_OK;
        manager->thread_data[i].running = true;
        if (pthread_create(&manager->threads[i], NULL, manager_loop, &manager->thread_data[i]) <
            0)
            return TSOCKET_THREAD;
    }
    return TSOCKET_OK;
}

/**
 * @defgroup threaded_socket_manager
 * @{
 */

/**
 * Creates a new manager.
 * Creates as many threads as there are cores in your computer.
 * As threads are pretty expensive, you should only create one manager and use that throughout your program.
 * @param threads How many threads to use. If this value is 0, it will use the same amount as the cores in your computer.
 * @param status A pointer to put the status of the function into
 * @return A new t_tsocket_manager
 */
t_tsocket_manager *new_tsocket_manager(s32 threads, t_tsocket_status *status) {
    t_tsocket_manager *manager;
    s32 cores;

    if (!status)
        return NULL;
    manager = g_malloc0(sizeof(t_tsocket_manager));
    if (!manager)
        return set_status_and_return(TSOCKET_MALLOC, status, NULL);
    manager->inited = true;
    if (threads <= 0) {
        cores = sysconf(_SC_NPROCESSORS_ONLN);
        if (cores < 0)
            cores = 4;
    } else
        cores = threads;
    manager->threads = g_malloc0(sizeof(pthread_t) * cores);
    if (!manager->threads)
        return set_status_and_return(TSOCKET_MALLOC, status, NULL);
    manager->thread_data = g_malloc0(sizeof(t_tsocket_manager_thread_data) * cores);
    if (!manager->thread_data)
        return set_status_and_return(TSOCKET_MALLOC, status, NULL);
    manager->threads_length = cores;
    *status = set_up_threads(manager);
    if (*status != TSOCKET_OK)
        return NULL;
    manager->inited = true;
    return set_status_and_return(TSOCKET_OK, status, manager);
}

/**
 * Returns if there was an error in one of the threads and resets the error.
 * @param manager
 * @return The error if there was one, otherwise TSOCKET_OK.
 */
t_tsocket_status check_error_tsocket_manager(t_tsocket_manager *manager) {
    u32 i;

    if (!manager)
        return TSOCKET_ARGS;
    for (i = 0; i < manager->threads_length; i++) {
        if (manager->thread_data[i].status != TSOCKET_OK)
            return manager->thread_data[i].status;
    }
    return TSOCKET_OK;
}

/**
 * Adds a client to the manager.
 * @param manager
 * @param sock
 * @return A status based on how things went.
 */
t_tsocket_status add_client_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock) {
    if (!manager || !sock)
        return TSOCKET_ARGS;
    if (!manager->inited)
        return TSOCKET_ARGS;
    lock_mutex(manager->thread_data[manager->current_array].mutex);
    g_ptr_array_add(manager->thread_data[manager->current_array].socks, sock);
    unlock_mutex(manager->thread_data[manager->current_array].mutex);
    manager->current_array++;
    if (manager->current_array >= manager->threads_length)
        manager->current_array = 0;
    return TSOCKET_OK;
}

/**
 * It will first try and find the socket in the manager, if the socket is in the manager it will remove it from the manager.
 * If the socket still has data to be sent, this function will block until all the data has been sent.
 * If it was not found, it will return TSOCKET_NO_DATA.
 * @param manager
 * @param sock Socket to remove
 * @return
 */
t_tsocket_status remove_client_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock) {
    u32 i, j;
    t_tsocket *arr_sock;

    for (i = 0; i < manager->threads_length; i++) {
        lock_mutex(manager->thread_data[i].mutex);
        for (j = 0; j < manager->thread_data[i].socks->len; j++) {
            arr_sock = g_ptr_array_index(manager->thread_data[i].socks, j);
            if (arr_sock == sock) {
                if (arr_sock->output->length > 0) {
                    unlock_mutex(manager->thread_data[i].mutex);
                    while (arr_sock->output->length > 0) {
                        delay(1);
                    }
                    lock_mutex(manager->thread_data[i].mutex);
                }
                g_ptr_array_remove_index(manager->thread_data[i].socks, j);
                unlock_mutex(manager->thread_data[i].mutex);
                return TSOCKET_OK;
            }
        }
        unlock_mutex(manager->thread_data[i].mutex);
    }
    return TSOCKET_NO_DATA;
}

/**
 * Returns which thread and location inside of that thread. This is mostly used internally.
 * @param manager
 * @param sock
 * @param iloc The thread it is located in.
 * @param jloc The location inside of the thread.
 * @return A status based on how things went.
 */
t_tsocket_status get_socket_location_tsocket_manager(t_tsocket_manager *manager,
                                                     t_tsocket *sock,
                                                     u32 *iloc,
                                                     u32 *jloc) {
    u32 i, j;
    t_tsocket *array_sock;

    if (!manager || !sock || !iloc)
        return TSOCKET_ARGS;
    if (!manager->inited)
        return TSOCKET_ARGS;
    for (i = 0; i < manager->threads_length; i++) {
        for (j = 0; j < manager->thread_data[i].socks->len; j++) {
            array_sock = g_ptr_array_index(manager->thread_data[i].socks, j);
            if (array_sock->id == sock->id && array_sock->sock.socket_fd == sock->sock.socket_fd) {
                *iloc = i;
                if (jloc)
                    *jloc = j;
                return TSOCKET_OK;
            }
        }
    }
    return TSOCKET_NO_DATA;
}

/**
 * Returns if that socket is currently locked.
 * @param manager
 * @param sock
 * @param status A status pointer that is set when things go wrong.
 * @return true if the socket is locked, false otherwise.
 */
bool socket_locked_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock, t_tsocket_status *status) {
    u32 i;

    if (!manager || !sock)
        return set_status_and_return(TSOCKET_ARGS, status, false);
    if (!manager->inited)
        return set_status_and_return(TSOCKET_ARGS, status, false);
    *status = get_socket_location_tsocket_manager(manager, sock, &i, NULL);
    if (*status != TSOCKET_OK)
        return false;
    *status = TSOCKET_OK;
    return is_locked_mutex(manager->thread_data[i].mutex);
}

/**
 * Waits until the thread that the socket is tied to is unlocked.
 * @param manager
 * @param sock
 * @return A status based on how things went.
 */
t_tsocket_status wait_until_socket_unlocked_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock) {
    t_tsocket_status status;
    bool locked;
    u32 i;

    if (!manager || !sock)
        return TSOCKET_ARGS;
    if (!manager->inited)
        return TSOCKET_ARGS;
    locked = socket_locked_tsocket_manager(manager, sock, &status);
    if (status != TSOCKET_OK)
        return status;
    if (locked) {
        status = get_socket_location_tsocket_manager(manager, sock, &i, NULL);
        if (status != TSOCKET_OK)
            return status;
        lock_mutex(manager->thread_data[i].mutex);
        unlock_mutex(manager->thread_data[i].mutex);
    }
    return TSOCKET_OK;
}

/**
 * Unlocks the thread attached to the socket. If the socket could not be found, it will return TSOCKET_NO_DATA.
 * @param manager
 * @param sock
 * @return A status based on how things went.
 */
t_tsocket_status lock_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock) {
    t_tsocket_status status;
    u32 i;

    status = get_socket_location_tsocket_manager(manager, sock, &i, NULL);
    if (status != TSOCKET_OK)
        return status;
    lock_mutex(manager->thread_data[i].mutex);
    return TSOCKET_OK;
}

/**
 * Unlocks the thread attached to the socket. If the socket could not be found, it will return TSOCKET_NO_DATA.
 * @param manager
 * @param sock
 * @return A status based on how things went.
 */
t_tsocket_status unlock_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock) {
    t_tsocket_status status;
    u32 i;

    status = get_socket_location_tsocket_manager(manager, sock, &i, NULL);
    if (status != TSOCKET_OK)
        return status;
    unlock_mutex(manager->thread_data[i].mutex);
    return TSOCKET_OK;
}

/**
 * Returns if the thread is currently active (Has data to send or receive)
 * @param manager
 * @param thread Which thread to check
 * @return
 */
static bool threads_active_tsocket_manager(t_tsocket_manager *manager) {
    t_tsocket *sock;
    t_tsocket_manager_thread_data *data;
    u32 i, j;
    bool active;

    active = false;
    for (i = 0; i < manager->threads_length; i++) {
        data = &manager->thread_data[i];
        for (j = 0; j < data->socks->len; j++) {
            sock = g_ptr_array_index(data->socks, j);
            if (sock->output->length > 0) {
                active = true;
                break;
            }
        }
    }
    return active;
}

/**
 * Stops all the threads and frees the manager.
 * Will block until all the data has been sent and received.
 * @param manager
 */
void free_tsocket_manager(t_tsocket_manager *manager) {
    u32 i;

    if (!manager)
        return;
    if (manager->threads) {
        while (threads_active_tsocket_manager(manager))
            delay(1);
        for (i = 0; i < manager->threads_length; i++) {
            manager->thread_data[i].running = false;
            pthread_join(manager->threads[i], NULL);
        }
        g_free(manager->threads);
    }
    if (manager->thread_data) {
        for (i = 0; i < manager->threads_length; i++) {
            free_mutex(manager->thread_data[i].mutex);
            g_ptr_array_free(manager->thread_data[i].socks, true);
        }
        g_free(manager->thread_data);
    }
    g_free(manager);
}

/**
* @}
*/

/**
 * Loop for each thread. Goes through each socket and updates that socket.
 * @param p
 * @return
 */
void *manager_loop(void *p) {
    t_tsocket_manager_thread_data *data;
    t_tsocket *sock;
    t_tsocket_status status;
    u64 start, end;
    u32 i;

    data = (t_tsocket_manager_thread_data *) p;
    if (!data)
        return NULL;
    while (data->running) {
        start = get_current_time();
        for (i = 0; i < data->socks->len; i++) {
            lock_mutex(data->mutex);
            sock = g_ptr_array_index(data->socks, i);
            status = tsocket_update(NULL, sock);
            if (status != TSOCKET_OK) {
                unlock_mutex(data->mutex);
                return set_status_and_return(status, &data->status, NULL);
            }
            unlock_mutex(data->mutex);
        }
        end = get_current_time();
        if (end - start < ITERATION_TIME)
            delay(ITERATION_TIME - (end - start));
    }
    return set_status_and_return(TSOCKET_OK, &data->status, NULL);
}

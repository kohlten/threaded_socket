/**
 * @defgroup threaded_socket_time
 * @{
 */

#ifndef THREADED_SOCKET_TIME_H
#define THREADED_SOCKET_TIME_H

#include "number.h"

/**
 * Gets the current time from the epoch in MS.
 * @return MS since the epoc.
 */
u64 get_current_time(void);

/**
 * Delays for MS
 * @param ms How long to wait
 */
void delay(u64 ms);

#endif //THREADED_SOCKET_TIME_H

/**
 * @}
 */
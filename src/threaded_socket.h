/**
 * @defgroup threaded_socket
 * @{
 */

#ifndef THREADED_SOCKET_H
#define THREADED_SOCKET_H

#ifdef C2NIM
#  lib threaded_socket
#  importc
#  if defined(windows)
#    define threaded_socketlib "threaded_socket.lib"
#  elif defined(macosx)
#    define threaded_socketlib "libthreaded_socket.a"
#  else
#    define threaded_socketlib "libthreaded_socket.a"
#  endif
#endif

#include <stdbool.h>
#include <pthread.h>

#include <glib-2.0/glib.h>

#include "number.h"
#include "socket.h"
#include "threaded_socket_time.h"
#include "threaded_socket_mutex.h"
#include "threaded_socket_init.h"
#include "threaded_socket_manager.h"
#include "threaded_socket_data.h"


#define MAJOR 0
#define MINOR 0
#define BUILD 5

// Unfortunately the manager functions need to be put here or we cannot get the manager struct.

/**
 * Adds unterminated data to queue to be sent.
 * @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
 * @param sock The socket
 * @param data Data to be sent
 * @param length Length of the data
 * @return Other status if failed, or TSOCKET_OK if succeeded.
 */
t_tsocket_status send_unterminated_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, const u8 *data, u64 length);

/**
 * Adds terminated data to queue to be sent.
 * @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
 * @param sock The socket
 * @param data Data to be sent
 * @return Other status if failed, or TSOCKET_OK if succeeded.
 */
t_tsocket_status send_terminated_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, const u8 *data);

/**
 * If there is data received, return it.
 * Otherwise set the status to TSOCKET_NO_DATA and return NULL.
 * @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
 * @param sock The socket
 * @param status Status pointer
 * @return NULL if failed, or the current data.
 */
t_tsocket_status peek_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, u8 *data, u64 bytes);

/**
 * If there is data received, return it and reset the buffer.
 * Otherwise set the status to TSOCKET_NO_DATA and return NULL.
 * @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
 * @param sock The socket
 * @param status Status pointer
 * @return NULL if failed, or the current data.
 */
u8 *get_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, u64 *bytes, t_tsocket_status *status);

/**
 * Receives any data currently in the socket and sends any data waiting to be sent.
 * @param manager If you are using a manager, pass it here to lock the variables.
 * @param sock
 * @return A status on how things went
 */
t_tsocket_status tsocket_update(t_tsocket_manager *manager, t_tsocket *sock);

/**
 * @return The current version in a temporary string.
 */
u8 *get_version_string(void);


#endif //THREADED_SOCKET_H

/**
 * @}
 */

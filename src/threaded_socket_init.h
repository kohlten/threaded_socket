//
// Created by kohlten on 4/29/19.
//

#ifndef THREADED_SOCKET_INIT_H
#define THREADED_SOCKET_INIT_H

#include "socket.h"
#include "threaded_socket_data.h"

/**
 * Creates a client from a server
 * @param client Client sock to bind to
 * @param server Server socket to bind from
 * @return TSOCKET_OK if everything went ok, otherwise return error
 */
t_tsocket *init_tsocket_client_from_server(t_socket *server, t_tsocket_status *status);

/**
 * Create new client
 * @param address Address to bind to
 * @param port Port to bind to
 * @return TSOCKET_OK if everything went ok, otherwise return error
 */
t_tsocket *init_tsocket_client(const s8 *address, u16 port, t_tsocket_status *status);

#endif //THREADED_SOCKET_INIT_H

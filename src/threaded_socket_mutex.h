/**
 * @defgroup threaded_socket_mutex
 * @{
 */

#ifndef THREADED_SOCKET_THREAD_H
#define THREADED_SOCKET_THREAD_H

#ifdef __APPLE__
#include <dispatch/dispatch.h>
#else
#include <semaphore.h>
#endif
#include <stdbool.h>

struct s_tsocket_mutex {
#ifdef __APPLE__
    dispatch_semaphore_t sem;
#else
    sem_t sem;
#endif
    bool is_locked;
};

typedef struct s_tsocket_mutex t_tsocket_mutex;

/**
 * Creates a new mutex and initialized the semaphore.
 * @return NULL if failed, or the new mutex if successful.
 */
t_tsocket_mutex *new_mutex(void);

/**
 * Locks the semaphore.
 * If the semaphore is already locked, it will wait until it is unlocked.
 * @param mutex
 */
void lock_mutex(t_tsocket_mutex *mutex);

/**
 * Unlocks the semaphore.
 * @param mutex
 */
void unlock_mutex(t_tsocket_mutex *mutex);

/**
 * Returns if the semaphore is currently locked.
 * @param mutex
 * @return true if the semaphore is locked, false otherwise.
 */
bool is_locked_mutex(t_tsocket_mutex *mutex);

/**
 * Destroys the semaphore and the mutex.
 * @param mutex_ptr
 */
void free_mutex(t_tsocket_mutex *mutex_ptr);

#endif //THREADED_SOCKET_THREAD_H

/**
 * @}
 */

#define TSOCKET_INTERNAL

/**
 * @defgroup threaded_socket
 * @{
 */

#include "threaded_socket.h"
#include "threaded_socket_time.h"
#include "threaded_socket_mutex.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Receives any data currently in the socket and sends any data waiting to be sent.
 * @param manager If you are using a manager, pass it here to lock the variables.
 * @param sock
 * @return A status on how things went
 */
t_tsocket_status tsocket_update(t_tsocket_manager *manager, t_tsocket *sock) {
    u8 packet[MAX_PACKET_SIZE];
    struct s_packet_data *packet_str;
    t_tsocket_status status;
    u64 bytes;

    if (!sock)
        return TSOCKET_ARGS;
    if (manager) {
        status = lock_tsocket_manager(manager, sock);
        if (status != TSOCKET_OK)
            return status;
    }
    receive_data_socket(sock->sock.socket_fd, MAX_PACKET_SIZE, (s8 *) packet, &bytes);
    if (bytes > 0 && packet[0] != 0) {
        if (sock->input->str == NULL) {
            sock->input = g_string_new(NULL);
            if (!sock->input)
                return TSOCKET_MALLOC;
        }
        if (g_string_append_len(sock->input, (char *) packet, bytes) == NULL)
            return TSOCKET_MALLOC;
    }
    while (sock->output->length > 0) {
        packet_str = g_queue_pop_head(sock->output);
        if (packet_str) {
            if (packet_str->length > MAX_PACKET_SIZE) {
                if (packet_str->data)
                    free(packet_str->data);
                free(packet_str);
                continue;
            }
            send_data_socket(sock->sock.socket_fd, (const s8 *) packet_str->data, packet_str->length);
            free(packet_str->data);
            free(packet_str);
        }
    }
    if (manager) {
        status = unlock_tsocket_manager(manager, sock);
        if (status != TSOCKET_OK)
            return status;
    }
    return TSOCKET_OK;
}

/**
 * @}
 */
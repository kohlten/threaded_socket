//
// Created by kohlten on 4/29/19.
//

#ifndef THREADED_SOCKET_DATA_H
#define THREADED_SOCKET_DATA_H

#include <glib.h>

#include "number.h"
#include "socket.h"

/**
 * Max size to receive a packet.
 */
#define MAX_PACKET_SIZE 1024

enum e_tsocket_status {
    TSOCKET_NO_DATA = 1,
    TSOCKET_ARGS = 2,
    TSOCKET_BIND = 3,
    TSOCKET_MALLOC = 4,
    TSOCKET_THREAD = 5,
    TSOCKET_BLOCKED = 6,
    TSOCKET_MORE_DATA = 7,
    TSOCKET_OK = 0
};

struct s_tsocket {
    t_socket sock;
    GQueue *output;
    GString *input;
    u64 id;
};

typedef enum e_tsocket_status t_tsocket_status;
typedef struct s_tsocket t_tsocket;

void *set_status_and_return(t_tsocket_status status, t_tsocket_status *status_ptr, void *value);

/**
 * Returns the amount of bytes currently available.
 * @param sock The socket
 * @return How many bytes currently available.
 */
u64 data_available_tsocket(t_tsocket *sock);

/**
 * Closes the socket, and frees the queue and vector.
 * @param sock Socket to destroy
 * @return TSOCKET_ARGS if the socket is null, TSOCKET_OK otherwise
 */
t_tsocket_status destroy_tsocket(t_tsocket *sock);

/**
 * Returns the string version of the tsocket status
 * @param status tsocket status
 * @return String version of status
 */
const u8 *tsocket_status_string(t_tsocket_status status);

/**
 * Returns the socket of a tsocket
 * @param sock tsocket
 * @return t_socket
 */
t_socket *get_tsocket_socket(t_tsocket *sock);

/**
 * Returns the socket file descriptor.
 * @param sock tSocket
 * @return File descriptor
 */
u64 get_tsocket_fd(t_tsocket *sock);

/**
 * Returns the address the socket is currently bound to. If not bound, will return garbage.
 * This function will malloc for the total size and return that. Use this if you want a thread safe copy.
 * @param sock Socket
 * @param status Status pointer
 * @return Address in address:port form
 */
u8 *get_tsocket_address(t_tsocket *sock, t_tsocket_status *status);

/**
 * Returns the address the socket is currently bound to. If not bound, will return garbage.
 * This function will malloc for the total size and return that. Use this if you want a temporary not thread safe copy.
 * @param sock Socket
 * @param status Status pointer
 * @return Address in address:port form
 */
u8 *get_tsocket_address_tmp(t_tsocket *sock);

#ifdef TSOCKET_INTERNAL

struct s_packet_data {
    u8 *data;
    u64 length;
};

#endif


#endif //THREADED_SOCKET_DATA_H

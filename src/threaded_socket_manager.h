//
// Created by kohlten on 4/26/19.
//

#ifndef THREADED_SOCKET_MANAGER_H
#define THREADED_SOCKET_MANAGER_H

#include "threaded_socket_mutex.h"
#include "threaded_socket_data.h"

#include <glib-2.0/glib.h>

/**
 * How many iterations for the loops to go through in a second.
 */
#define ITERATIONS_PER_SECOND 60
/**
 * How many MS for each iteration
 */
#define ITERATION_TIME 1000 / ITERATIONS_PER_SECOND

struct s_tsocket_manager_thread_data {
    GPtrArray *socks;
    t_tsocket_mutex *mutex;
    t_tsocket_status status;
    bool running;
};


/**
 * Loop for each thread. Goes through each socket and updates that socket.
 * @param p
 * @return
 */
void *manager_loop(void *p);


typedef struct s_tsocket_manager_thread_data t_tsocket_manager_thread_data;

struct s_tsocket_manager {
    u32 threads_length;
    u32 current_array;
    pthread_t *threads;
    t_tsocket_manager_thread_data *thread_data;
    bool inited;
};

typedef struct s_tsocket_manager t_tsocket_manager;

/**
 * Creates a new manager.
 * Creates as many threads as there are cores in your computer.
 * As threads are pretty expensive, you should only create one manager and use that throughout your program.
 * @param threads How many threads to use. If this value is 0, it will use the same amount as the cores in your computer.
 * @param status A pointer to put the status of the function into
 * @return A new t_tsocket_manager
 */
t_tsocket_manager *new_tsocket_manager(s32 threads, t_tsocket_status *status);

/**
 * Returns if there was an error in one of the threads and resets the error.
 * @param manager
 * @return The error if there was one, otherwise TSOCKET_OK.
 */
t_tsocket_status check_error_tsocket_manager(t_tsocket_manager *manager);

/**
 * Adds a client to the manager.
 * @param manager
 * @param sock
 * @return A status based on how things went.
 */
t_tsocket_status add_client_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock);

/**
 * Returns which thread and location inside of that thread. This is mostly used internally.
 * @param manager
 * @param sock
 * @param iloc The thread it is located in.
 * @param jloc The location inside of the thread.
 * @return A status based on how things went.
 */
t_tsocket_status get_socket_location_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock, u32 *iloc, u32 *jloc);

/**
 * Returns if that socket is currently locked.
 * @param manager
 * @param sock
 * @param status A status pointer that is set when things go wrong.
 * @return true if the socket is locked, false otherwise.
 */
bool socket_locked_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock, t_tsocket_status *status);

/**
 * Waits until the thread that the socket is tied to is unlocked.
 * @param manager
 * @param sock
 * @return A status based on how things went.
 */
t_tsocket_status wait_until_socket_unlocked_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock);

/**
 * Unlocks the thread attached to the socket. If the socket could not be found, it will return TSOCKET_NO_DATA.
 * @param manager
 * @param sock
 * @return A status based on how things went.
 */
t_tsocket_status lock_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock);

/**
 * Unlocks the thread attached to the socket. If the socket could not be found, it will return TSOCKET_NO_DATA.
 * @param manager
 * @param sock
 * @return A status based on how things went.
 */
t_tsocket_status unlock_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock);

t_tsocket_status remove_client_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock);

/**
 * Stops all the threads and frees the manager.
 * @param manager
 */
void free_tsocket_manager(t_tsocket_manager *manager);

#endif //THREADED_SOCKET_MANAGER_H

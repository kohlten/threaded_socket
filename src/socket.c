/**
 * @defgroup Socket
 * @{
 */

#include "socket.h"

#include <stdio.h>
#include <unistd.h>
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

/**
 * Set flags to the socket using fcntl.
 * @param socket Socket
 * @param flags Flags to set to socket
 * @return SOCKET_OK if everything went good, otherwise will send another status.
 */
socket_status set_flags_socket(t_socket *socket, int flags) {
    int flags_sock;
    int err;

    if (!socket || socket->socket_fd < 0)
        return SOCKET_ARGS;
    flags_sock = fcntl(socket->socket_fd, F_GETFL, 0);
    if (flags_sock == -1)
        return SOCKET_OPT;
    err = fcntl(socket->socket_fd, F_SETFL, flags);
    if (err != 0)
        return SOCKET_OPT;
    return SOCKET_OK;
}

/**
 * Create a new server on adress and port with a number of clients.
 * @param server Server socket.
 * @param address Address to bind to
 * @param port Port to bind to
 * @param clients How many clients
 * @return SOCKET_OK if everything went good, otherwise will send another status.
 */
socket_status new_server(t_socket *server, const s8 *address, u16 port, u32 clients) {
    s32 opt = 1;
    u64 size;

    if (!server || !address)
        return SOCKET_ARGS;
    bzero(server, sizeof(t_socket));
    server->socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server->socket_fd <= 0)
        return SOCKET_SOCKET;
    if (setsockopt(server->socket_fd, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof(opt)) < 0)
        return SOCKET_OPT;
    bzero(&server->address_info, sizeof(struct sockaddr_in));
    server->address_info.sin_family = AF_INET;
    server->address_info.sin_port = htons(port);
    if (inet_pton(AF_INET, (const char *) address, &server->address_info.sin_addr) <= 0)
        return SOCKET_IP;
    size = sizeof(struct sockaddr);
    if (bind(server->socket_fd, (struct sockaddr *) &server->address_info, (socklen_t) size) < 0)
        return SOCKET_BIND;
    if (listen(server->socket_fd, (s32) clients) < 0)
        return SOCKET_ARGS;
    return SOCKET_OK;
}

/**
 * Create a client from the server
 * @param client Client socket
 * @param server Server socket
 * @return SOCKET_OK if everything went good, otherwise will send another status.
 */
socket_status new_client_from_server(t_socket *client, t_socket *server) {
    s32 addr_len;

    if (!client || !server)
        return SOCKET_ARGS;
    bzero(client, sizeof(t_socket));
    addr_len = sizeof(server->address_info);
    client->socket_fd = accept(server->socket_fd, (struct sockaddr *) &server->address_info, (socklen_t *) &addr_len);
    if (client->socket_fd < 0)
        return SOCKET_SOCKET;
    client->address_info = server->address_info;
    return SOCKET_OK;
}

/**
 * Create a new client and connect to address
 * @param client Client socket
 * @param address Address to bind to
 * @param port Socket to bind to
 * @return SOCKET_OK if everything went good, otherwise will send another status.
 */
socket_status new_client(t_socket *client, const s8 *address, u16 port) {
    if (!client || !address)
        return SOCKET_ARGS;
    bzero(client, sizeof(t_socket));
    client->socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (client->socket_fd < 0)
        return SOCKET_SOCKET;
    client->address_info.sin_family = AF_INET;
    client->address_info.sin_port = htons(port);
    if (inet_pton(AF_INET, (const char *) address, &client->address_info.sin_addr) <= 0)
        return SOCKET_IP;
    if (connect(client->socket_fd, (struct sockaddr *) &client->address_info, sizeof(struct sockaddr_in)) < 0)
        return SOCKET_IP;
    return SOCKET_OK;
}

/**
 * Sends data over the socket
 * @param fd Socket file descriptor to send the data over.
 * @param data Data to send
 * @param data_size How many bytes the data is.
 * @return SOCKET_OK if everything went good, otherwise will send another status.
 */
socket_status send_data_socket(s32 fd, const s8 *data, u64 data_size) {
    if (fd < 0 || !data)
        return SOCKET_ARGS;
    send(fd, data, data_size, 0);
    return SOCKET_OK;
}

/**
 * Recieve data over the socket. Will put the bytes read into bytes_read when done.
 * @param fd Socket file descriptor to send the data over.
 * @param data_size How many bytes the data_ptr has allocated
 * @param data_ptr Array to put the data into
 * @param bytes_read How many bytes were read
 * @return SOCKET_OK if everything went good, otherwise will send another status.
 */
socket_status receive_data_socket(s32 fd, u64 data_size, s8 *data_ptr, u64 *bytes_read) {
    if (fd < 0 || !data_ptr)
        return SOCKET_ARGS;
    bzero(data_ptr, data_size);
    *bytes_read = (u64) read(fd, data_ptr, data_size);
    return SOCKET_OK;
}

s32 get_fd_socket(t_socket *sock) {
    return sock->socket_fd;
}

u8 *get_socket_address(t_socket *sock) {
    u8 *address;
    static u8 combined[31];

    if (!sock)
        return NULL;
    address = (u8 *) inet_ntoa(sock->address_info.sin_addr);
    sprintf((char *) combined, "%s:%d", address, sock->address_info.sin_port);
    return combined;
}

/**
 * Closes the socket
 * @param server Socket
 */
void close_socket(t_socket *server) {
    close(server->socket_fd);
}

/**
 * Returns the string representation of a status.
 * @param status The integer status
 * @return A string representation of the status
 */
const u8 *get_string_socket_status(socket_status status) {
    switch (status) {
        case SOCKET_SOCKET:
            return (const u8 *) "Failed to create socket.";
        case SOCKET_OPT:
            return (const u8 *) "Failed to set socket flags.";
        case SOCKET_IP:
            return (const u8 *) "Failed to bind to ip.";
        case SOCKET_PORT:
            return (const u8 *) "Failed to bind to the port.";
        case SOCKET_BIND:
            return (const u8 *) "Failed to bind with ip and port.";
        case SOCKET_ARGS:
            return (const u8 *) "One of the parameters is invalid.";
        case SOCKET_MALLOC:
            return (const u8 *) "Out of memory.";
        case SOCKET_OK:
            return (const u8 *) "All good.";
        default:
            return (const u8 *) "Unknown status.";
    }
}

/**
 * @}
 */

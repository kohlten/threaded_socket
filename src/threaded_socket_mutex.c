//
// Created by kohlten on 4/20/19.
//

#define TSOCKET_INTERNAL
#include "threaded_socket_mutex.h"

#include <stdlib.h>
#include <errno.h>
#include <glib-2.0/glib.h>

/**
 * @defgroup threaded_socket_mutex
 * @{
 */

/**
 * Creates a new mutex and initialized the semaphore.
 * @return NULL if failed, or the new mutex if successful.
 */
t_tsocket_mutex *new_mutex(void) {
    t_tsocket_mutex *mutex;

    mutex = g_malloc0(sizeof(t_tsocket_mutex));
    if (!mutex)
        return NULL;
#ifdef __APPLE__
    mutex->sem = dispatch_semaphore_create(1);
#else
    if (sem_init(&mutex->sem, 0, 1) < 0)
        return NULL;
#endif
    return mutex;
}

/**
 * Locks the semaphore.
 * If the semaphore is already locked, it will wait until it is unlocked.
 * @param mutex
 */
void lock_mutex(t_tsocket_mutex *mutex) {
#ifdef __APPLE__
    dispatch_semaphore_wait(mutex->sem, DISPATCH_TIME_FOREVER);
#else
    int r;

    do {
        r = sem_wait(&mutex->sem);
    } while (r == -1 && errno == EINTR);
#endif
    mutex->is_locked = true;
}

/**
 * Unlocks the semaphore.
 * @param mutex
 */
void unlock_mutex(t_tsocket_mutex *mutex) {
#ifdef __APPLE__
    dispatch_semaphore_signal(mutex->sem);
#else
    sem_post(&mutex->sem);
#endif
    mutex->is_locked = false;
}

/**
 * Returns if the semaphore is currently locked.
 * @param mutex
 * @return true if the semaphore is locked, false otherwise.
 */
bool is_locked_mutex(t_tsocket_mutex *mutex) {
    return mutex->is_locked;
}

/**
 * Destroys the semaphore and the mutex.
 * @param mutex_ptr
 */
void free_mutex(t_tsocket_mutex *mutex_ptr) {
    if (!mutex_ptr)
        return;
#ifdef __APPLE__
#else
    sem_destroy(&mutex_ptr->sem);
#endif
    free(mutex_ptr);
}

/**
 * @}
 */
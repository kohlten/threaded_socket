//import threaded_socket;
import threaded_socket.tsocket;
import std.stdio;
import std.conv;
//import core.stdc.stdlib;
import std.string;

int main() {
    Socket server;
    TSocket client;
    Manager manager;

    server = new Socket("0.0.0.0", 8181, 1);
    manager = new Manager(1);
    client = new TSocket(server);

    manager.AddClient(client);
    while (client.DataAvailable() < 18) {}
    auto data = client.ReceiveData(&manager);
    writeln(data);
    client.SendData(&manager, "Hello from server!");
    /*t_tsocket_manager *manager;
    t_tsocket_status status;
    t_tsocket client;
    u8 *data;
    u64 start;
    u64 bytes;

    manager = new_tsocket_manager(&status);
    if (status != t_tsocket_status.TSOCKET_OK)
      return error(tsocket_status_string(status), cast(int)status);
    status = init_tsocket_client(&client, cast(const s8 *)toStringz("0.0.0.0"), 8181);
    if (status != t_tsocket_status.TSOCKET_OK)
      return error(tsocket_status_string(status), cast(int)status);
    status = add_client_tsocket_manager(manager, &client);
    if (status != t_tsocket_status.TSOCKET_OK)
      return error(tsocket_status_string(status), status);
    start = get_current_time();
    send_terminated_data_tsocket(manager, &client, cast(u8 *)toStringz("Hello from client!"));
    while (data_available_tsocket(&client) < 18) {
        status = check_error_tsocket_manager(manager);
        if (status != t_tsocket_status.TSOCKET_OK)
            return error(cast(u8 *)toStringz("Took too long!"), -1);
        // If we have gone over TIMEOUT, it took too long and we failed.
        if (get_current_time() - start > TIMEOUT)
            return error(cast(u8 *)toStringz("Took too long!"), -1);
    }
    data = get_data_tsocket(manager, &client, &bytes, &status);
    if (status != t_tsocket_status.TSOCKET_OK)
        return error(tsocket_status_string(status), status);
    writeln(fromStringz(cast(char *)data));
    free(data);
    free_tsocket_manager(manager);
    destroy_tsocket(&client);*/
    return 0;
}

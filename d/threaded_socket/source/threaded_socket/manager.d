module threaded_socket.manager;

import threaded_socket.tsocket, threaded_socket.number, threaded_socket.socket;

import core.sys.posix.pthread;
import std.string;

version(OSX) {
  import core.sys.darwin.mach.semaphore;
} else version (linux) {
  import core.sys.posix.semaphore;
}

struct t_tsocket_manager {
  u32 threads_length;
  u32 current;
  void *threads;
  void *thread_data;
  bool inited;
}

extern (C) {
t_tsocket_manager *new_tsocket_manager(s32 threads, TSocketStatus *status);
TSocketStatus check_error_tsocket_manager(t_tsocket_manager *manager);
TSocketStatus add_client_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock);
TSocketStatus get_socket_location_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock, u32 *iloc, u32 *jloc);
bool socket_locked_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock, TSocketStatus *status);
TSocketStatus wait_until_socket_unlocked_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock);
TSocketStatus lock_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock);
TSocketStatus unlock_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock);
TSocketStatus remove_client_tsocket_manager(t_tsocket_manager *manager, t_tsocket *sock);
void free_tsocket_manager(t_tsocket_manager *manager);
}

class Manager {
  private t_tsocket_manager *manager;

  this(s32 threads) {
    TSocketStatus status;

    this.manager = new_tsocket_manager(threads, &status);
    if (status != TSocketStatus.OK) {
      auto statusMessage = tsocket_status_string(status);
      throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
    }
  }

  void AddClient(TSocket sock) {
    TSocketStatus status;

    status = add_client_tsocket_manager(this.manager, sock.GetInternalTSocket());
    if (status != TSocketStatus.OK) {
      auto statusMessage = tsocket_status_string(status);
      throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
    }
  }

  void RemoveClient(TSocket sock) {
    TSocketStatus status;

    status = remove_client_tsocket_manager(this.manager, sock.GetInternalTSocket());
    if (status != TSocketStatus.OK) {
      auto statusMessage = tsocket_status_string(status);
      throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
    }
  }

  t_tsocket_manager *GetInternalManager() {
    return this.manager;
  }

  ~this() {
    free_tsocket_manager(manager);
  }
}

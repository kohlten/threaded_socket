module threaded_socket.tsocket;

public import threaded_socket.socket, threaded_socket.manager, threaded_socket.number;

import std.string, core.stdc.stdlib;

enum TSocketStatus {
    NO_DATA = 1,
    ARGS = 2,
    BIND = 3,
    MALLOC = 4,
    THREAD = 5,
    BLOCKED = 6,
    MORE_DATA = 7,
    OK = 0
}

struct GString {
  char *str;
  size_t len;
  size_t allocated_len;
}

struct GList {
  void *data;
  GList *next;
  GList *prev;
}

struct GQueue {
  GList *head;
  GList *tail;
  u32 length;
}

struct t_tsocket {
  t_socket sock;
  GQueue *output;
  GString *input;
  u64 id;
}


extern (C) {
  t_tsocket *init_tsocket_client_from_server(t_socket *server, TSocketStatus *status);
  t_tsocket *init_tsocket_client(const s8 *address, u16 port, TSocketStatus *status);
  u64 data_available_tsocket(t_tsocket *sock);
  TSocketStatus destroy_tsocket(t_tsocket *sock);
  u8 *tsocket_status_string(TSocketStatus status);
  t_socket *get_tsocket_socket(t_tsocket *sock);
  u64 get_tsocket_fd(t_tsocket *sock);
  u8 *get_tsocket_address(t_tsocket *sock, TSocketStatus *status);
  u8 *get_tsocket_address_tmp(t_tsocket *sock);
  TSocketStatus send_unterminated_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, const u8 *data, u64 length);
  TSocketStatus send_terminated_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, const u8 *data);
  TSocketStatus peek_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, u8 *data, u64 bytes);
  u8 *get_data_tsocket(t_tsocket_manager *manager, t_tsocket *sock, u64 *bytes, TSocketStatus *status);
  TSocketStatus tsocket_update(t_tsocket_manager *manager, t_tsocket *sock);
  u8 *get_version_string();
}


class TSocket {
  private t_tsocket *sock;

  // init_tsocket_client_from_server
  this(Socket server) {
    TSocketStatus status;

    this.sock = init_tsocket_client_from_server(server.GetInternalSocket(), &status);
    if (!sock || status != TSocketStatus.OK) {
      auto statusMessage = tsocket_status_string(status);
      throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
    }
  }

  this(const string address, u16 port) {
    TSocketStatus status;

    this.sock = init_tsocket_client(cast(const s8 *)toStringz(address), port, &status);
    if (!sock || status != TSocketStatus.OK) {
      auto statusMessage = tsocket_status_string(status);
      throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
    }
  }

  t_tsocket *GetInternalTSocket() {
    return this.sock;
  }

  u64 DataAvailable() {
    return data_available_tsocket(sock);
  }

  void SendData(Manager *manager, string data) {
      TSocketStatus status;
      t_tsocket_manager *manager_c = null;

      if (manager)
        manager_c = (*manager).GetInternalManager();
      status = send_terminated_data_tsocket(manager_c, this.sock, cast(const u8 *)toStringz(data));
      if (status != TSocketStatus.OK) {
          auto statusMessage = tsocket_status_string(status);
          throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
      }
  }

  u8[] ReceiveData(Manager *manager) {
      TSocketStatus status;
      u64 bytes;
      t_tsocket_manager *manager_c = null;

      if (manager)
        manager_c = (*manager).GetInternalManager();
      auto data_c = get_data_tsocket(manager_c, this.sock, &bytes, &status);
      if (status != TSocketStatus.OK) {
          auto statusMessage = tsocket_status_string(status);
          throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
      }
      auto data = cast(u8[])fromStringz(cast(char *)data_c);
      free(data_c);
      return data;
  }

  u8[] PeekData(Manager *manager, u64 size) {
    TSocketStatus status;
    auto data = new u8[size];
    t_tsocket_manager *manager_c = null;

    if (manager)
      manager_c = (*manager).GetInternalManager();
    status = peek_data_tsocket(manager_c, this.sock, data.ptr, size);
    if (status != TSocketStatus.OK) {
        auto statusMessage = tsocket_status_string(status);
        throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
    }
    return data;
  }

  ~this() {
    destroy_tsocket(sock);
  }
}

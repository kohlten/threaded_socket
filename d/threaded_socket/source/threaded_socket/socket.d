module threaded_socket.socket;

import threaded_socket.number;

import core.sys.posix.netinet.in_;
import std.string;

struct t_socket {
    s32 socket_fd;
    sockaddr_in address_info;
}

enum SocketStatus {
    SOCKET = 1,
    OPT = 2,
    IP = 3,
    PORT = 4,
    BIND = 5,
    ARGS = 6,
    MALLOC = 7,
    FD = 8,
    OK = 0
}

extern(C) {
    SocketStatus new_server(t_socket *server, const s8 *address, u16 port, u32 clients);
    SocketStatus new_client_from_server(t_socket *client, t_socket *server);
    SocketStatus new_client(t_socket *socket_p, const s8 *address, u16 port);
    SocketStatus send_data_socket(s32 fd, const s8 *data, u64 data_size);
    SocketStatus receive_data_socket(s32 fd, u64 data_size, s8 *data_ptr, u64 *bytes_read);
    SocketStatus set_flags_socket(t_socket *socket, int flags);
    s32 get_fd_socket(t_socket *sock);
    u8 *get_socket_address(t_socket *sock);
    u8 *get_string_socket_status(SocketStatus status);
    void close_socket(t_socket *client);
}


public class Socket {
    private t_socket sock;

    // New Server
    this(const string address, u16 port, u32 clients) {
        SocketStatus status;

        status = new_server(&this.sock, cast(const s8 *)toStringz(address), port, clients);
        if (status != SocketStatus.OK) {
            auto statusMessage = get_string_socket_status(status);
            throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
        }
    }

    // New client from server
    this(Socket server) {
        SocketStatus status;

        status = new_client_from_server(&this.sock, server.GetInternalSocket());
        if (status != SocketStatus.OK) {
            auto statusMessage = get_string_socket_status(status);
            throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
        }
    }

    // New client
    this(const string address, u16 port) {
        SocketStatus status;

        status = new_client(&this.sock, cast(const s8 *)toStringz(address), port);
        if (status != SocketStatus.OK) {
            auto statusMessage = get_string_socket_status(status);
            throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
        }
    }

    void SendData(string data) {
        SocketStatus status;

        status = send_data_socket(get_fd_socket(&this.sock), cast(const s8 *)toStringz(data), data.length);
        if (status != SocketStatus.OK) {
            auto statusMessage = get_string_socket_status(status);
            throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
        }
    }

    s8[] ReceiveData(u64 size) {
        SocketStatus status;
        u64 bytes;
        auto data = new s8[size];

        status = receive_data_socket(get_fd_socket(&this.sock), size, data.ptr, &bytes);
        if (status != SocketStatus.OK) {
            auto statusMessage = get_string_socket_status(status);
            throw new StringException(cast(string)fromStringz(cast(char *)statusMessage));
        }
        data = data[0 .. bytes];
        return data;
    }

    t_socket *GetInternalSocket() {
        return &this.sock;
    }
}

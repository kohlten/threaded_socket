#!/bin/bash
set -e
meson build
sudo ninja -C build all install

currentDir=$(pwd)

if [[ $(command -v nim) != "" ]]
then
	cp src/*.h build/libthreaded_socket.a nim/src
	cd nim && nimble install
fi

if [[ $(command -v go) != "" ]] && [[ $GOPATH != "" ]]
then
	echo "Installing for go"
	url=$(git remote show origin | grep "Fetch URL: *" | cut -c22-)
	cd $currentDir
	mkdir -p $GOPATH/src/$url
	cd go && cp -rf threaded_socket/* $GOPATH/src/$url
	go install $url
fi

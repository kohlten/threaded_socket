# Package

version       = "0.0.5"
author        = "Alex Strole"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"

# Dependencies

requires "nim >= 0.19.4"

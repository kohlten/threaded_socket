## *
##  @defgroup threaded_socket
##  @{
##

proc getPath(): string {.compileTime.} =
    staticExec("pwd")

proc getGlib(): string {.compileTime.} =
    staticExec("pkg-config --cflags glib-2.0")

{.passC: "-I " & getPath() & " " & getGlib().}
{.passC: "-Wall -Wextra".}
{.passC: ""}
{.passL: "-L" & getPath() & " -lthreaded_socket -lglib-2.0 -lpthread".}

## *
##  Max size to receive a packet.
##

const
  MAX_PACKET_SIZE* = 1024

const
  MAJOR* = 0
  MINOR*  = 0
  BUILD*  = 5

import
  number, socket, threaded_socket_time, threaded_socket_init,
  threaded_socket_manager, threaded_socket_data

##  Unfortunately the manager functions need to be put here or we cannot get the manager struct.
## *
##  Adds unterminated data to queue to be sent.
##  @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
##  @param sock The socket
##  @param data Data to be sent
##  @param length Length of the data
##  @return Other status if failed, or TSOCKET_OK if succeeded.
##

proc send_unterminated_data_tsocket*(manager: ptr t_tsocket_manager;
                                    sock: ptr t_tsocket; data: ptr u8; length: u64): t_tsocket_status {.importc.}
## *
##  Adds terminated data to queue to be sent.
##  @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
##  @param sock The socket
##  @param data Data to be sent
##  @return Other status if failed, or TSOCKET_OK if succeeded.
##

proc send_terminated_data_tsocket*(manager: ptr t_tsocket_manager;
                                  sock: ptr t_tsocket; data: ptr u8): t_tsocket_status {.importc.}
## *
##  If there is data received, return it.
##  Otherwise set the status to TSOCKET_NO_DATA and return NULL.
##  @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
##  @param sock The socket
##  @param status Status pointer
##  @return NULL if failed, or the current data.
##

proc peek_data_tsocket*(manager: ptr t_tsocket_manager; sock: ptr t_tsocket;
                       bytes: ptr u64; status: ptr t_tsocket_status): ptr u8 {.importc.}
## *
##  If there is data received, return it and reset the buffer.
##  Otherwise set the status to TSOCKET_NO_DATA and return NULL.
##  @param manager Pass the t_tsocket_manager struct here if you are using a manager to lock the thread.
##  @param sock The socket
##  @param status Status pointer
##  @return NULL if failed, or the current data.
##

proc get_data_tsocket*(manager: ptr t_tsocket_manager; sock: ptr t_tsocket;
                      bytes: ptr u64; status: ptr t_tsocket_status): ptr u8 {.importc.}
## *
##  Receives any data currently in the socket and sends any data waiting to be sent.
##  @param manager If you are using a manager, pass it here to lock the variables.
##  @param sock
##  @return A status on how things went
##

proc tsocket_update*(manager: ptr t_tsocket_manager; sock: ptr t_tsocket): t_tsocket_status  {.importc.}

##
## @return The current version in a temporary string.
##
proc get_version_string*(): ptr u8 {.importc.}
## *
##  @}
##

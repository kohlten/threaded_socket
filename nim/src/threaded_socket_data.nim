##
##  Created by kohlten on 4/29/19.
##

import
  number, socket

## *
##  Max size to receive a packet.
##

const
  MAX_PACKET_SIZE* = 1024

type
  e_tsocket_status* = enum
    TSOCKET_OK = 0,
    TSOCKET_NO_DATA = 1,
    TSOCKET_ARGS = 2,
    TSOCKET_BIND = 3,
    TSOCKET_MALLOC = 4,
    TSOCKET_THREAD = 5,
    TSOCKET_BLOCKED = 6

type
  t_tsocket* {.importc: "struct s_tsocket", header: "threaded_socket_data.h", bycopy.} = object
    ##sock*: t_socket
    ##output*: ptr GQueue
    ##input*: ptr GString

  t_tsocket_status* = e_tsocket_status


## *
##  Returns the amount of bytes currently available.
##  @param sock The socket
##  @return How many bytes currently available.
##

proc data_available_tsocket*(sock: ptr t_tsocket): u64 {.importc.}
## *
##  Closes the socket, and frees the queue and vector.
##  @param sock Socket to destroy
##  @return TSOCKET_BLOCKED if there is still data to be sent, TSOCKET_OK otherwise
##

proc destroy_tsocket*(sock: ptr t_tsocket): t_tsocket_status =
  {.emit: "result = destroy_tsocket(`sock`);".}
## *
##  Returns the string version of the tsocket status
##  @param status tsocket status
##  @return String version of status
##

proc tsocket_status_string*(status: t_tsocket_status): ptr u8 {.importc.}
## *
##  Returns the socket of a tsocket
##  @param sock tsocket
##  @return t_socket
##

proc get_tsocket_socket*(sock: ptr t_tsocket): ptr t_socket {.importc.}
## *
##  Returns the socket file descriptor.
##  @param sock tSocket
##  @return File descriptor
##

proc get_tsocket_fd*(sock: ptr t_tsocket): u64 {.importc.}
## *
##  Returns the address the socket is currently bound to. If not bound, will return garbage.
##  This function will malloc for the total size and return that. Use this if you want a thread safe copy.
##  @param sock Socket
##  @param status Status pointer
##  @return Address in address:port form
##

proc get_tsocket_address*(sock: ptr t_tsocket; status: ptr t_tsocket_status): ptr u8 {.importc.}
## *
##  Returns the address the socket is currently bound to. If not bound, will return garbage.
##  This function will malloc for the total size and return that. Use this if you want a temporary not thread safe copy.
##  @param sock Socket
##  @param status Status pointer
##  @return Address in address:port form
##

proc get_tsocket_address_tmp*(sock: ptr t_tsocket): ptr u8 {.importc.}
when defined(TSOCKET_INTERNAL):
  type
    s_packet_data* {.bycopy.} = object
      data*: ptr u8
      length*: u64

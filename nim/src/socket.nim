## *
##  @defgroup Socket
##  @{
##

import
  number

type
  s_socket* {.importc: "struct s_socket", header: "socket.h", bycopy.} = object
    ##socket_fd*: s32            ## * File descriptor
    ## * Address binded to
    ##address_info*: {.importc: "struct sockaddr_in", header: "arpa/inet.h", bycopy.}

  e_socket_status* = enum
    SOCKET_OK = 0, SOCKET_SOCKET = 1, SOCKET_OPT = 2, SOCKET_IP = 3, SOCKET_PORT = 4,
    SOCKET_BIND = 5, SOCKET_ARGS = 6, SOCKET_MALLOC = 7, SOCKET_FD = 8


type
  t_socket* = s_socket
  socket_status* = e_socket_status


proc new_server*(server: ptr t_socket; address: ptr s8; port: u16; clients: u32): socket_status = {.emit: "result = new_server(`server`, `address`, `port`, `clients`);".}
proc new_client_from_server*(client: ptr t_socket; server: ptr t_socket): socket_status  {.importc.}
proc new_client*(socket_p: ptr t_socket; address: ptr s8; port: u16): socket_status {.importc.}
proc send_data_socket*(fd: s32; data: ptr s8; data_size: u64): socket_status {.importc.}
proc receive_data_socket*(fd: s32; data_size: u64; data_ptr: ptr s8; bytes_read: ptr u64): socket_status {.importc.}
proc set_flags_socket*(socket: ptr t_socket; flags: cint): socket_status {.importc.}
proc get_string_socket_status*(status: socket_status): ptr u8 {.importc.}
proc close_socket*(client: ptr t_socket) {.importc.}
## *
##  @}
##

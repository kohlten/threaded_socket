##
##  Created by kohlten on 4/29/19.
##

import
  number, socket, threaded_socket_data

## *
##  Creates a client from a server
##  @param client Client sock to bind to
##  @param server Server socket to bind from
##  @return TSOCKET_OK if everything went ok, otherwise return error
##

proc init_tsocket_client_from_server*(client: ptr t_tsocket; server: ptr t_socket): t_tsocket_status =
  {.emit: "result = init_tsocket_client_from_server(`client`, `server`);".}
## *
##  Create new client
##  @param client Client socket to bind to
##  @param address Address to bind to
##  @param port Port to bind to
##  @return TSOCKET_OK if everything went ok, otherwise return error
##

proc init_tsocket_client*(client: ptr t_tsocket; address: ptr s8; port: u16): t_tsocket_status =
  {.emit: "result = init_tsocket_client(`client`, `address`, `port`);".}

##
##  Created by kohlten on 4/26/19.
##

import
  threaded_socket_mutex, threaded_socket_data, number

## *
##  How many iterations for the loops to go through in a second.
##

const
  ITERATIONS_PER_SECOND* = 500

## *
##  How many MS for each iteration
##

const
  ITERATION_TIME* = 1000 div ITERATIONS_PER_SECOND

type
  s_tsocket_manager_thread_data* {.importc: "struct s_tsocket_manager_thread_data", header: "threaded_socket_manager.h", bycopy.} = object
    ##socks*: ptr GPtrArray
    ##mutex*: ptr t_tsocket_mutex
    ##status*: t_tsocket_status
    ##running*: bool
  t_tsocket_manager_thread_data* = s_tsocket_manager_thread_data


## *
##  Loop for each thread. Goes through each socket and updates that socket.
##  @param p
##  @return
##

proc manager_loop*(p: pointer): pointer {.importc.}

type
  s_tsocket_manager* {.importc: "struct s_tsocket_manager", header: "threaded_socket_manager.h", bycopy.} = object
    ##threads_length*: u32
    ##current_array*: u32
    ##threads*: ptr pthread_t
    ##thread_data*: ptr t_tsocket_manager_thread_data
    ##inited*: bool
  t_tsocket_manager* = s_tsocket_manager

## *
##  Creates a new manager.
##  Creates as many threads as there are cores in your computer.
##  As threads are pretty expensive, you should only create one manager and use that throughout your program.
##  @param status A pointer to put the status of the function into
##  @return A new t_tsocket_manager
##

proc new_tsocket_manager*(status: ptr t_tsocket_status): ptr t_tsocket_manager =
  {.emit: "result = new_tsocket_manager(`status`);".}
## *
##  Returns if there was an error in one of the threads and resets the error.
##  @param manager
##  @return The error if there was one, otherwise TSOCKET_OK.
##

proc check_error_tsocket_manager*(manager: ptr t_tsocket_manager): t_tsocket_status {.importc.}
## *
##  Adds a client to the manager.
##  @param manager
##  @param sock
##  @return A status based on how things went.
##

proc add_client_tsocket_manager*(manager: ptr t_tsocket_manager; sock: ptr t_tsocket): t_tsocket_status =
  {.emit: "result = add_client_tsocket_manager(`manager`, `sock`);".}
## *
##  Returns which thread and location inside of that thread. This is mostly used internally.
##  @param manager
##  @param sock
##  @param iloc The thread it is located in.
##  @param jloc The location inside of the thread.
##  @return A status based on how things went.
##

proc get_socket_location_tsocket_manager*(manager: ptr t_tsocket_manager;
    sock: ptr t_tsocket; iloc: ptr u32; jloc: ptr u32): t_tsocket_status {.importc.}
## *
##  Returns if that socket is currently locked.
##  @param manager
##  @param sock
##  @param status A status pointer that is set when things go wrong.
##  @return true if the socket is locked, false otherwise.
##

proc socket_locked_tsocket_manager*(manager: ptr t_tsocket_manager;
                                   sock: ptr t_tsocket;
                                   status: ptr t_tsocket_status): bool {.importc.}
## *
##  Waits until the thread that the socket is tied to is unlocked.
##  @param manager
##  @param sock
##  @return A status based on how things went.
##

proc wait_until_socket_unlocked_tsocket_manager*(manager: ptr t_tsocket_manager;
    sock: ptr t_tsocket): t_tsocket_status {.importc.}
## *
##  Unlocks the thread attached to the socket. If the socket could not be found, it will return TSOCKET_NO_DATA.
##  @param manager
##  @param sock
##  @return A status based on how things went.
##

proc lock_tsocket_manager*(manager: ptr t_tsocket_manager; sock: ptr t_tsocket): t_tsocket_status {.importc.}
## *
##  Unlocks the thread attached to the socket. If the socket could not be found, it will return TSOCKET_NO_DATA.
##  @param manager
##  @param sock
##  @return A status based on how things went.
##

proc unlock_tsocket_manager*(manager: ptr t_tsocket_manager; sock: ptr t_tsocket): t_tsocket_status {.importc.}
## *
##  Stops all the threads and frees the manager.
##  @param manager
##

proc free_tsocket_manager*(manager: ptr t_tsocket_manager) {.importc.}

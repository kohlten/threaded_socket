## *
##  @defgroup threaded_socket_mutex
##  @{
##

type
  s_tsocket_mutex* {.importc: "struct s_tsocket_mutex", header: "threaded_socket_mutex.h", bycopy.} = object
    ##sem*: sem_t
    ##is_locked*: bool

  t_tsocket_mutex* = s_tsocket_mutex

## *
##  Creates a new mutex and initialized the semaphore.
##  @return NULL if failed, or the new mutex if successful.
##

proc new_mutex*(): ptr t_tsocket_mutex {.importc.}
## *
##  Locks the semaphore.
##  If the semaphore is already locked, it will wait until it is unlocked.
##  @param mutex
##

proc lock_mutex*(mutex: ptr t_tsocket_mutex) {.importc.}
## *
##  Unlocks the semaphore.
##  @param mutex
##

proc unlock_mutex*(mutex: ptr t_tsocket_mutex) {.importc.}
## *
##  Returns if the semaphore is currently locked.
##  @param mutex
##  @return true if the semaphore is locked, false otherwise.
##

proc is_locked_mutex*(mutex: ptr t_tsocket_mutex): bool {.importc.}
## *
##  Destroys the semaphore and the mutex.
##  @param mutex_ptr
##

proc free_mutex*(mutex_ptr: ptr t_tsocket_mutex) {.importc.}
## *
##  @}
##

type
  s8* = cchar
  s16* = cshort
  s32* = cint
  s64* = clonglong
  u8* = cuchar
  u16* = cushort
  u32* = cuint
  u64* = culonglong

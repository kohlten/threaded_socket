## *
##  @defgroup threaded_socket_time
##  @{
##

import
  number

## *
##  Gets the current time from the epoch in MS.
##  @return MS since the epoc.
##

proc get_current_time*(): u64 {.importc.}
## *
##  Delays for MS
##  @param ms How long to wait
##

proc delay*(ms: u64) {.importc.}
## *
##  @}
##

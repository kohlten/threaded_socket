package main

import (
	"fmt"
	"os"
	"gitlab.com/kohlten/threaded_socket"
)

const TIMEOUT = 10 * 1000

func error(message string, level int) {
	fmt.Println(message)
	os.Exit(level)
}

func main() {
	manager, status := threaded_socket.NewTSocketManager(1)
	defer manager.Free()
	if status != threaded_socket.TSocketOk {
		error(threaded_socket.TSocketStatusString(status), 1)
	}
	client, status := threaded_socket.InitTSocketClient("0.0.0.0", 8181)
	if status != threaded_socket.TSocketOk {
		error(threaded_socket.TSocketStatusString(status), 1)
	}
	status = manager.AddClient(client)
	if status != threaded_socket.TSocketOk {
		error(threaded_socket.TSocketStatusString(status), 1)
	}
	client.SendData(manager, []byte("Hello from client!"))
	start := threaded_socket.GetCurrentTime()
	for client.DataAvailable() < 18 {
		if threaded_socket.GetCurrentTime() - start > TIMEOUT {
			error(fmt.Sprintf("It's been %d MS and there has not been any data!\n",
				threaded_socket.GetCurrentTime() - start), 5)
		}
	}
	data, status := client.GetData(manager)
	if status != threaded_socket.TSocketOk {
		error(threaded_socket.TSocketStatusString(status), 6)
	}
	fmt.Println(data)
}

#include "threaded_socket.h"
#include "threaded_socket_time.h"
#include <stdio.h>
#include <stdlib.h>

#define TIMEOUT 10 * 1000

int error(const u8 *string, int err) {
    dprintf(2, "[%d]: %s\n", err, (const char *)string);
    return err;
}

int main() {
    t_tsocket_status status;
    t_tsocket client;
    u64 bytes;
    //u64 start;
    u8 *data;

    // Connect to 127.0.0.1:8181
    status = init_tsocket_client(&client, (const s8 *)"0.0.0.0", 8181);
    if (status != TSOCKET_OK)
        return error(tsocket_status_string(status), status);
    // Send data to the server
    send_terminated_data_tsocket(NULL, &client, (u8 *)"Hello from client!");
    //start = get_current_time();
    // Wait until we get data back
    while (data_available_tsocket(&client) < 18) {
        // Run the update function to get input from the client and send our data
        status = tsocket_update(NULL, &client);
        if (status != TSOCKET_OK)
            return error(tsocket_status_string(status), status);
        // If we have gone over TIMEOUT, it took too long and we failed.
        //if (get_current_time() - start > TIMEOUT)
        //    return error((const u8 *)"Took too long!", -1);
    }
    // Receive the data from the server
    data = get_data_tsocket(NULL, &client, &bytes, &status);
    if (status != TSOCKET_OK)
        return error(tsocket_status_string(status), status);
    printf("%s\n", data);
    // Have to free the data
    free(data);
    // Close the client socket
    destroy_tsocket(&client);
	return 0;
}
#include "threaded_socket.h"
#include "threaded_socket_time.h"
#include <stdio.h>
#include <stdlib.h>

#define TIMEOUT 10 * 1000

int error(const u8 *string, int err) {
    dprintf(2, "[%d]: %s\n", err, (const char *)string);
    return err;
}

int main() {
    t_socket server;
    t_tsocket client;
    s32 status;
    u64 bytes;
    u64 start;
    u8 *data;

    // Create a new server with an ip of 127.0.0.1:8181 (equivalent to 0.0.0.0:8181)
    status = new_server(&server, (const s8 *)"0.0.0.0", 8181, 1) ;
    if (status != SOCKET_OK)
        return error(tsocket_status_string(status), status);
    // Wait for a client to connect and create a new client when the client connects
    status = init_tsocket_client_from_server(&client, &server);
    if (status != TSOCKET_OK)
        return error(tsocket_status_string(status), status);
    start = get_current_time();
    // Wait for the data from the client
    while (data_available_tsocket(&client) < 18) {
        // Run the input function to get input from the client
        status = tsocket_update(NULL, &client);
        if (status != TSOCKET_OK)
            return error(tsocket_status_string(status), status);
        // If we have gone over TIMEOUT, it took too long and we failed.
        if (get_current_time() - start > TIMEOUT)
            return error((const u8 *)"Took too long!", -1);
    }
    // Receive the data from the client
    data = get_data_tsocket(NULL, &client, &bytes, (t_tsocket_status *)&status);
    if (status != TSOCKET_OK)
        return error(tsocket_status_string(status), status);
    // Send data to the client
    send_terminated_data_tsocket(NULL, &client, (u8 *)"Hello from server!");
    // Run the output function to send all current data
    status = tsocket_update(NULL, &client);
    if (status != TSOCKET_OK)
        return error(tsocket_status_string(status), status);
    printf("%s\n", data);
    // Have to free the data
    free(data);
    // Close the server and client sockets
    destroy_tsocket(&client);
    close_socket(&server);
	return 0;
}
//
// Created by kohlten on 5/2/19.
//

#include <assert.h>
#include <stdio.h>

#include "threaded_socket.h"

int main() {
    printf("%s\n", get_version_string());
    assert(strcmp((char *)get_version_string(), "0.0.5") == 0);
    return 0;
}
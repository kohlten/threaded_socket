#include "threaded_socket.h"
#include "threaded_socket_time.h"
#include "threaded_socket_manager.h"
#include <stdio.h>
#include <stdlib.h>

#define TIMEOUT 10 * 1000

int error(const u8 *string, int err) {
    dprintf(2, "[%d]: %s\n", err, (const char *)string);
    return err;
}

// Using the manager allows us to no longer have to run
// those pesky input and output functions all the time.

int main() {
    t_tsocket_status status;
    t_tsocket client;
    t_tsocket_manager *manager;
    u64 bytes;
    u64 start;
    u8 *data;

    // Create a new socket manager
    manager = new_tsocket_manager(&status);
    if (!manager || status != TSOCKET_OK)
        return error(tsocket_status_string(status), status);
    status = init_tsocket_client(&client, (const s8 *)"0.0.0.0", 8181);
    if (status != TSOCKET_OK)
        return error(tsocket_status_string(status), status);
    status = add_client_tsocket_manager(manager, &client);
    if (status != TSOCKET_OK)
        return error(tsocket_status_string(status), status);
    // Send data to the server
    send_terminated_data_tsocket(manager, &client, (u8 *)"Hello from client!");
    start = get_current_time();
    // Wait until we get data back
    while (data_available_tsocket(&client) < 18) {
        status = check_error_tsocket_manager(manager);
        if (status != TSOCKET_OK)
            return error((const u8 *)"Took too long!", -1);
        // If we have gone over TIMEOUT, it took too long and we failed.
        if (get_current_time() - start > TIMEOUT)
            return error((const u8 *)"Took too long!", -1);
    }
    // Receive the data from the server
    data = get_data_tsocket(manager, &client, &bytes, &status);
    if (status != TSOCKET_OK)
        return error(tsocket_status_string(status), status);
    printf("%s\n", data);
    // Have to free the data
    free(data);
    free_tsocket_manager(manager);
    // Close the client socket
	return 0;
}
import threaded_socket, threaded_socket_init,
  threaded_socket_data, threaded_socket_manager,
  threaded_socket_time, socket, number,
  system, strformat

const TIMEOUT = 10 * 1000

proc error(message: string, exit_value: int) =
  echo message
  quit(exit_value)

proc main() =
  var status: t_tsocket_status
  var manager: ptr t_tsocket_manager
  var client: t_tsocket
  var start: u64
  var data: ptr u8
  var bytes: u64

  status = init_tsocket_client(addr(client), cast[ptr u8]("0.0.0.0"), 8181)
  if status != TSOCKET_OK:
    error(&"Failed to create client with error {status}", 1)
  manager = new_tsocket_manager(addr(status));
  if status != TSOCKET_OK:
    error(&"Failed to create manager with error {status}", 1)
  status = add_client_tsocket_manager(manager, addr(client))
  if status != TSOCKET_OK:
    error(&"Failed to add client to manager with error {status}", 1)
  discard send_terminated_data_tsocket(manager, addr(client), cast[ptr u8]("Hello from client!"));
  start = get_current_time()
  while data_available_tsocket(addr(client)) < 18:
    if get_current_time() - start > cast[u64](TIMEOUT):
        error("Took too long!\n", 1)
  data = get_data_tsocket(manager, addr(client), addr(bytes), addr(status))
  if status != TSOCKET_OK:
    error(&"Failed to get the data with error {status}", 1)
  echo data
  free_tsocket_manager(manager)
  discard destroy_tsocket(addr(client))

when isMainModule:
  main()

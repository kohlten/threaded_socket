# Package

version       = "0.1.0"
author        = "Alex Strole"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
bin           = @["server"]


# Dependencies

requires "nim >= 0.19.4"
requires "threaded_socket >= 0.0.5"
import
  threaded_socket, threaded_socket_init,
  threaded_socket_data, threaded_socket_manager,
  threaded_socket_time, socket, number,
  system, strformat

const TIMEOUT = 10 * 1000

proc error(message: string, exit_value: int) =
  echo message
  quit(exit_value)

proc main() =
  var status: t_tsocket_status
  var manager = threaded_socket_manager.new_tsocket_manager(addr(status))
  if status != TSOCKET_OK:
    error(&"Failed to create manager with error {status}", 1)
  var server: t_socket
  var status_s = new_server(addr(server), cast[ptr u8]("0.0.0.0"), 8181, 1)
  if status_s != SOCKET_OK:
    error(&"Failed to create server with error {status_s}", 1)
  var client: t_tsocket
  status = init_tsocket_client_from_server(addr(client), addr(server));
  if status != TSOCKET_OK:
    echo &"Failed to create client with error {status}"
    quit(1)
  status = add_client_tsocket_manager(manager, addr(client))
  if status != TSOCKET_OK:
    error(&"Failed to add client to manager with error {status}", 1)
  let start = get_current_time()
  while data_available_tsocket(addr(client)) < 18:
    if get_current_time() - start > cast[u64](TIMEOUT):
        error("Took too long!\n", 1)
  var bytes: u64
  let data = get_data_tsocket(manager, addr(client), addr(bytes), addr(status))
  if status != TSOCKET_OK:
    error(&"Failed to get the data with error {status}", 1)
  echo data
  discard send_terminated_data_tsocket(manager, addr(client), cast[ptr u8]("Helllo from server!"))
  discard destroy_tsocket(addr(client))
  close_socket(addr(server))
  free_tsocket_manager(manager)

when isMainModule:
  main()

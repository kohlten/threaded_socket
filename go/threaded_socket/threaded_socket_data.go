package threaded_socket

// #cgo pkg-config: threaded_socket glib-2.0
// #include <stdlib.h>
// #include "number.h"
// #include "socket.h"
// #include "threaded_socket_data.h"
import "C"
import "unsafe"

const (
	TSocketNoData  = 1
	TSocketArgs    = 2
	TSocketBind    = 3
	TSocketMalloc  = 4
	TSocketThread  = 5
	TSocketOk      = 0
)

type TSocketStatus = C.enum_e_tsocket_status
type TSocket = C.struct_s_tsocket

func (sock *TSocket) DataAvailable() uint64 {
	return uint64(C.data_available_tsocket(sock))
}

func (sock *TSocket) Destroy() {
	C.destroy_tsocket(sock)
}

func TSocketStatusString(status TSocketStatus) string {
	switch status {
	case TSocketNoData:
		return "There is no data waiting."
	case TSocketArgs:
		return "Invalid arguments."
	case TSocketMalloc:
		return "Out of memory."
	case TSocketThread:
		return "Failed to create thread."
	case TSocketBind:
		return "Failed to bind to ip and port. Is there another process running on that port?"
	case TSocketOk:
		return "All good."
	default:
		return "Unknown status."
	}
}

func (sock *TSocket) GetSocket() *Socket {
	return C.get_tsocket_socket(sock)
}

func (sock *TSocket) GetFd() uint64 {
	return uint64(C.get_tsocket_fd(sock))
}

func (sock *TSocket) GetAddress() string {
	address := C.get_tsocket_address_tmp(sock)
	return C.GoString((*C.char)(unsafe.Pointer(address)))
}

package threaded_socket

// #cgo pkg-config: threaded_socket glib-2.0
// #include <stdlib.h>
// #include "number.h"
// #include "socket.h"
// #include "threaded_socket_mutex.h"
// #include "threaded_socket_data.h"
// #include "threaded_socket_manager.h"
import "C"

type TSocketManager = C.struct_s_tsocket_manager

func NewTSocketManager(threads int32) (*TSocketManager, TSocketStatus) {
	var status TSocketStatus
	manager := C.new_tsocket_manager(C.s32(threads), (*C.t_tsocket_status)(&status))
	return manager, status
}

func (manager *TSocketManager)CheckError() TSocketStatus {
	status := C.check_error_tsocket_manager(manager)
	return uint32(status)
}

func (manager *TSocketManager)AddClient(sock *TSocket) TSocketStatus {
	status := C.add_client_tsocket_manager(manager, sock)
	return uint32(status)
}

func (manager *TSocketManager)RemoveClient(sock *TSocket) TSocketStatus {
	status := C.remove_client_tsocket_manager(manager, sock)
	return uint32(status)
}

func (manager *TSocketManager)GetSocketLocation(sock *TSocket) (uint32, uint32, TSocketStatus) {
	var iloc uint32
	var jloc uint32
	status := uint32(C.get_socket_location_tsocket_manager(manager, sock, (*C.u32)(&iloc), (*C.u32)(&jloc)))
	return iloc, jloc, status
}

func (manager *TSocketManager)SocketLocked(sock *TSocket) (bool, TSocketStatus) {
	var status TSocketStatus
	locked := bool(C.socket_locked_tsocket_manager(manager, sock, (*C.t_tsocket_status)(&status)))
	return locked, status
}

func (manager *TSocketManager)LockTSocket(sock *TSocket) TSocketStatus {
	status := C.lock_tsocket_manager(manager, sock)
	return uint32(status)
}

func (manager *TSocketManager)UnlockTSocket(sock *TSocket) TSocketStatus {
	status := C.unlock_tsocket_manager(manager, sock)
	return uint32(status)
}

func (manager *TSocketManager)Free() {
	C.free_tsocket_manager(manager)
}

package threaded_socket

// #cgo pkg-config: threaded_socket
// #include "number.h"
import "C"

type S8 = C.s8
type S16 = C.s16
type S32 = C.s32
type S64 = C.s64

type U8 = C.u8
type U16 = C.u16
type U32 = C.u32
type U64 = C.u64

package threaded_socket

// #cgo pkg-config: threaded_socket
// #include "threaded_socket_mutex.h"
import "C"

type TSocketMutex = C.struct_s_tsocket_mutex

func NewMutex() *TSocketMutex {
  return C.new_mutex()
}

func (mutex *TSocketMutex)Lock() {
  C.lock_mutex(mutex)
}

func (mutex *TSocketMutex)Unlock() {
  C.unlock_mutex(mutex)
}

func (mutex *TSocketMutex)IsLocked() bool {
  return bool(C.is_locked_mutex(mutex))
}

func (mutex *TSocketMutex)Free() {
  C.free_mutex(mutex)
}

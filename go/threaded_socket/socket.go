/**
 * @defgroup Socket
 * @{
 */

package threaded_socket

// #cgo pkg-config: threaded_socket glib-2.0
// #include <stdlib.h>
// #include "number.h"
// #include "socket.h"
import "C"
import (
  "unsafe"
)

type Socket = C.struct_s_socket
type SocketStatus = C.socket_status

const (
  SocketSocket = 1
  SocketOpt    = 2
  SocketIp     = 3
  SocketPort   = 4
  SocketBind   = 5
  SocketArgs   = 6
  SocketMalloc = 7
  SocketOk     = 0
)

func NewServer(address string, port uint16, clients uint32) (Socket, SocketStatus) {
  var server Socket
  cAddress := (*C.s8)(C.CBytes([]byte(address)))
  defer C.free(unsafe.Pointer(cAddress))
  status := C.new_server(&server, cAddress, C.u16(port), C.u32(clients))
  return server, status
}

func NewClientFromServer(server *Socket) (Socket, SocketStatus) {
  var client Socket
  status := C.new_client_from_server(&client, server)
  return client, status
}

func NewClient(address string, port uint16) (Socket, SocketStatus) {
  var client Socket
  cAddress := (*C.s8)(C.CBytes([]byte(address)))
  defer C.free(unsafe.Pointer(cAddress))
  status := C.new_client(&client, cAddress, C.u16(port))
  return client, status
}

func (socket *Socket)SendData(data []byte) SocketStatus {
  cData := (*C.s8)(C.CBytes(data))
  defer C.free(unsafe.Pointer(cData))
  return C.send_data_socket(C.s32(socket.GetFd()), cData, C.u64(len(data)))
}

func (socket *Socket)ReceiveData(dataSize uint64) ([]byte, SocketStatus) {
  var cBytes C.u64
  cData := (*C.s8)(C.malloc(C.ulong(dataSize * C.sizeof_s8)))
  defer C.free(unsafe.Pointer(cData))
  status := C.receive_data_socket(C.s32(socket.GetFd()), C.u64(dataSize), cData, &cBytes)
  return C.GoBytes(unsafe.Pointer(cData), C.int(cBytes)), status
}

func (socket *Socket)SetFlags(flags int32) SocketStatus {
  return C.set_flags_socket(socket, C.int(flags))
}

func (socket *Socket)GetFd() int32 {
    fd := C.get_fd_socket(socket)
    return int32(fd)
}

func (socket *Socket)GetAddress() string {
  address := C.get_socket_address(socket)
  return C.GoString((*C.char)(unsafe.Pointer(address)))
}

func (socket *Socket)Close() {
  C.close_socket(socket)
}

func GetStringSocketStatus(status SocketStatus) string {
  switch status {
  case SocketSocket:
    return "Failed to create socket."
  case SocketOpt:
    return "Failed to set socket flags."
  case SocketIp:
    return "Failed to bind to ip."
  case SocketPort:
    return "Failed to bind to the port."
  case SocketBind:
    return "Failed to bind with ip and port."
  case SocketArgs:
    return "One of the parameters is invalid."
  case SocketMalloc:
    return "Out of memory."
  case SocketOk:
    return "All good."
  default:
    return "Unknown status."
  }
}

package threaded_socket

// #cgo pkg-config: threaded_socket glib-2.0
// #include <stdlib.h>
// #include "number.h"
// #include "socket.h"
// #include "threaded_socket.h"
import "C"
import "unsafe"

const (
  MAJOR = 0
  MINOR = 0
  BUILD = 5
)

func (sock *TSocket)SendData(manager *TSocketManager, data []byte) TSocketStatus {
  cData := (*C.u8)(unsafe.Pointer(C.CBytes(data)))
  defer C.free(unsafe.Pointer(cData))
  status := C.send_unterminated_data_tsocket(manager, sock, cData, C.u64(len(data)))
  return uint32(status)
}

func (sock *TSocket)PeekData(manager *TSocketManager) ([]byte, TSocketStatus) {
  bytes := sock.DataAvailable()
  data := unsafe.Pointer(C.g_malloc0(C.ulong(bytes + 1)))
  defer C.free(unsafe.Pointer(data))
  status := C.peek_data_tsocket(manager, sock, (*C.u8)(data), C.u64(bytes))
  if status != TSocketOk {
    return nil, uint32(status)
  }
  if unsafe.Pointer(data) == C.NULL {
    return nil, uint32(TSocketMalloc)
  }
  goData := C.GoBytes(unsafe.Pointer(data), C.int(bytes))
  return goData, uint32(status)
}

func (sock *TSocket)GetData(manager *TSocketManager) ([]byte, TSocketStatus) {
  var bytes C.u64
  var status C.t_tsocket_status
  data := C.get_data_tsocket(manager, sock, &bytes, &status)
  if status != TSocketOk {
    return nil, uint32(status)
  }
  defer C.free(unsafe.Pointer(data))
  return C.GoBytes(unsafe.Pointer(data), C.int(bytes)), uint32(status)
}

func (sock *TSocket)Update(manager *TSocketManager) TSocketStatus {
  status := C.tsocket_update(manager, sock)
  return uint32(status)
}

func Free(p unsafe.Pointer) {
  C.free(p)
}

func GetVersionString() string {
  ver := C.GoString((*C.char)(unsafe.Pointer(C.get_version_string())))
  return ver
}
